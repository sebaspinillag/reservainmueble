/*global QUnit*/

sap.ui.define([
	"ui5/jm/ReservaInmueble/controller/Reservainmueble.controller"
], function (Controller) {
	"use strict";

	QUnit.module("Reservainmueble Controller");

	QUnit.test("I should test the Reservainmueble controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});