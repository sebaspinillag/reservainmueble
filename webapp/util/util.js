sap.ui.define([
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/core/BusyIndicator"
], function (MessageBox, MessageToast, BusyIndicator) {
	"use strict";

	return {

		console: console,

		_showBI: function (value) {
			if (value) {
				BusyIndicator.show(0);
			} else {
				BusyIndicator.hide();
			}
		},

		_onShowMessage: function (_message, _type) {
			try {
				if (_message !== undefined && _type !== undefined) {
					if (_type === "info") {
						MessageBox.information(_message);
					} else if (_type === "error") {
						MessageBox.error(_message);
					} else if (_type === "warn") {
						MessageBox.warning(_message);
					} else if (_type === "toast") {
						MessageToast.show(_message);
					} else if (_type === "done") {
						MessageBox.success(_message);
					}
				} else {
					this.console.warn("_message or _type are undefined");
				}
			} catch (err) {
				this.console.warn(err.stack);
			}
		}

	};

});