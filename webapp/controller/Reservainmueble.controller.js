sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"jquery.sap.global",
	"ui5/jm/ReservaInmueble/util/util",
	"ui5/jm/ReservaInmueble/controller/Asignarunidad/Utils"
], function (Controller, JSONModel, Utils) {
	"use strict";

	return Controller.extend("ui5.jm.ReservaInmueble.controller.Reservainmueble", {

		hana: new sap.ui.model.odata.ODataModel("/hana/logical/services/services.xsodata"),
		erp: new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZHOJANEGOCIO_SRV/"),
		c4c: new sap.ui.model.odata.ODataModel("/c4c_dev/sap/c4c/odata/v1/c4codataapi/"),
		xsjs: "/hana/logical/services/",

		onInit: function () {
			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({
				pattern: "yyyy-MM-dd",
				calendarType: sap.ui.core.CalendarType.Gregorian
			});
			this.oNumberFormatter = sap.ui.core.format.NumberFormat.getFloatInstance({
				groupingEnabled: true,
				minFractionDigits: 0,
				decimalSeparator: "."
			});
			var oModel = new JSONModel({
				unidades: [],
				unidadesMod: [],
				unidadesSel: [],
				historial: [],
				torres: [],
				tipoIdentificacion: [],
				resumenInv: [],
				pisos: [],
				reserva: {
					ID_RESERVA: 1,
					PROYECTO: "",
					ETAPA: "",
					TORRE: "",
					LEAD_IDENTIFICACION: null,
					LEAD_NOMBRE: "",
					ASESOR: null,
					ESTADO_RESERVA: null,
					FECHA_INICIAL: null,
					FECHA_FINAL: null,
					FECHA_CREACION: new Date(),
					MEDIO: "",
					ID_OPORTUNIDAD: 0
				},
				combosData: {
					projects: [],
					asesor: [],
					estados: [],
					unity_type: [],
					torres: []
				}
			});
			this.getView().setModel(oModel);
			this.leerProyectos();
			this.tipoIdentificacion();
		},
		//Función para bloquear la pantalla mientras carga los datos
		showBusyDialog: function (fFunction) {
			window.iPassedTimedInTicks = 0;
			sap.ui.core.BusyIndicator.show(0);
			jQuery.sap.delayedCall(50, this, function () {
				try {
					fFunction();
				} catch (oException) {
					console.log("Ha ocurrido un error", oException);
				}
				this.hideBusyIndicator();
			});
		},
		//Función para desbloquear la pantalla mientras carga los datos
		hideBusyIndicator: function () {
			sap.ui.core.BusyIndicator.hide();
		},
		//Función para el formateo de miles.
		currencyFormatter: function (iValue) {
			return "$ " + this.oNumberFormatter.format(iValue);
		},
		//Función para cargar los tipos de unidades disponibles desde SCP.
		unityType: function () {
			var oModel = this.getView().getModel();
			var aProjects = [];
			var proyecto = this.getView().byId("cmbProyecto").getSelectedKey();
			var etapa = this.getView().byId("cmbProyecto").getSelectedItem().getText();
			if (etapa) {
				etapa = "0".concat(etapa.substr(etapa.length - 1, 1));
				proyecto = parseFloat(proyecto.concat(etapa));
			}
			jQuery.ajax({
				url: this.xsjs + "xsjs/general/get_unity_type.xsjs?PROY=" + proyecto,
				method: 'GET',
				dataType: 'json',
				async: false,
				success: function (result) {
					aProjects = result;
				},
				error: function (err) {
					console.log(["Error actualizando el motivo: ", err]);
				}
			});
			oModel.setProperty("/combosData/unity_type", aProjects);
		},
		//Función para realizar filtro en el las unidades disponibles.
		filterUnities: function (oEvent) {
			var tblMaster = sap.ui.getCore().byId("table").getBinding("items");
			var sValue = oEvent.getSource().getValue();
			var oFilter2 = new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({
						path: 'adicional/Texto',
						operator: sap.ui.model.FilterOperator.Contains,
						value1: sValue
					})
				],
				or: true
			});
			tblMaster.filter([oFilter2]);
		},
		//Función para cargar los proyectos disponibles desde SCP.
		leerProyectos: function () {
			var oModel = this.getView().getModel();
			var aProjects = [];
			var sSelectFields = "$select= PRODH, VTEXT, MONEDA_PROYECTO";
			var sFilter = "$filter= STUFE eq '1'";
			this.hana.read("T_T179?" + sSelectFields + "&" + sFilter, null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						aProjects = oData.results;
					} else {
						util._onShowMessage("La tabla T_T179 se encuentra sin información", "warn");
					}
				});
			oModel.setProperty("/combosData/projects", aProjects);
		},
		//Función para cargar los estados disponibles desde SCP.
		leerEstados: function () {
			var oModel = this.getView().getModel();
			var aEstados = [];
			this.hana.read("T_RESERVA_ESTADOS", null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						aEstados = oData.results;
					}
				});
			oModel.setProperty("/combosData/estados", aEstados);
		},
		//Función para cargar los Empleados disponibles desde SCP.
		leerAsesores: function () {
			var oModel = this.getView().getModel();
			var aAsesor = [];
			var proyecto = this.getView().byId("cmbProyecto").getSelectedKey();
			var sSelectFields = "$select= CODIGO_INTERLOCUTOR, NOMBRE";
			var sFilter = "$filter= ID_PROYECTO eq '" + proyecto + "' and FUNCION_INTERLOC eq 'VE'";
			this.hana.read("T_EMPLEADOSHN?" + sSelectFields + "&" + sFilter, null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						aAsesor = oData.results;
					} else {
						util._onShowMessage("La tabla T_EMPLEADOSHN se encuentra sin información", "warn");
					}
				});
			oModel.setProperty("/combosData/asesor", aAsesor);
		},
		//Función para consultar las torres asociadas al PROYECTO-ETAPA.
		consultarTorres: function () {
			this.LeerClientes();
			var oModel = this.getView().getModel();
			var aTorres = [];
			var proyecto = this.getView().byId("cmbProyecto").getSelectedKey();
			var etapa = this.getView().byId("cmbProyecto").getSelectedItem().getText();
			if (etapa) {
				etapa = "0".concat(etapa.substr(etapa.length - 1, 1));
				proyecto = parseFloat(proyecto.concat(etapa));
			}
			var sFilter = "$filter= startswith(PRODH,'" + proyecto + "') and STUFE eq '6'";
			this.hana.read("T_JERARQUIA?" + sFilter, null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						aTorres = oData.results;
					}
				});
			oModel.setProperty("/combosData/torres", aTorres);
		},
		//Función para mostrar las unidades de acuerdo al piso seleccionado.
		MostrarUnidades: function (oEvent) {
			var oModel = this.getView().getModel();
			var UnidadesAp = [];
			var that = this;
			var piso = oEvent.getParameters().listItem.getCells()[0].getText();
			var oUnidades = oModel.getProperty("/unidades");
			for (var i = 0; i < oUnidades.length; i++) {
				if (piso === oUnidades[i].adicional.Pisot) {
					var tipo;
					var numeroR = that.consultarFechaReserva(oUnidades[i].adicional.Matnr);
					if (numeroR.length === 2 || numeroR.length === 1 || numeroR.length === 3) {
						tipo = "Emphasized";
					}
					var unidadesVendidas = that.validateSoldUnities(oUnidades[i].adicional.Matnr);
					if (unidadesVendidas > 0) {
						tipo = "Reject";
					} else if (unidadesVendidas === 0 && numeroR.length === 0) {
						tipo = "Accept";
					}
					var obj = {
						texto: oUnidades[i].adicional.Texto,
						material: oUnidades[i].adicional.Matnr,
						type: tipo
					};
					UnidadesAp.push(obj);
				}
			}
			this.getView().byId("Unidades").setVisible(true);
			console.log(UnidadesAp);
			oModel.setProperty("/filtroUnidades", UnidadesAp);
		},
		//Consultar las unidades disponibles para el modal de Asignación.
		UnidadesModal: function (oEvent) {
			var aUnities = [],
				oModel = this.getView().getModel();
			var sProjectId = this.getView().byId("cmbProyecto").getSelectedKey();
			var etapa = this.getView().byId("cmbProyecto").getSelectedItem().getText();
			var matkl = oEvent.getSource().getSelectedKey();
			if (etapa) {
				etapa = "0".concat(etapa.substr(etapa.length - 1, 1));
				sProjectId = sProjectId.concat(etapa);
			}
			var that = this;
			this.showBusyDialog(function () {
				var sFilter = "$filter= Proye eq '" + sProjectId +
					//"' and (Matkl eq 'Z00800001' or Matkl eq 'Z00800002' or Matkl eq 'Z00800005' or Matkl eq 'Z00800006' or Matkl eq 'Z00800007')";
					"' and Matkl eq '" + matkl + "'";
				that.erp.read("DisponibilidadSet?" + sFilter + "&sap-language=ES", null, null, false,
					function (oData) {
						$.each(oData.results, function (index, oUnity) {
							oUnity.Netwr = Number(oUnity.Netwr);
							oUnity.Matnr = '00000000' + oUnity.Matnr;
							oUnity.Area = oUnity.Area.split(",").join(".");
							aUnities[index] = {
								selected: false,
								adicional: oUnity
							};
						});
					}
				);
				oModel.setProperty("/unidadesMod", aUnities);
			});
		},
		//Función para traer las unidades disponibles del ERP.
		CargarUnidades: function () {
			var aUnities = [],
				oModel = this.getView().getModel();
			var sProjectId = this.getView().byId("cmbProyecto").getSelectedKey();
			var etapa = this.getView().byId("cmbProyecto").getSelectedItem().getText();
			if (!this.getView().byId("cmbTorre").getSelectedItem()) {
				sap.m.MessageBox.warning("Por favor diligencie el campo Torre.");
				return;
			}
			var Torre = this.getView().byId("cmbTorre").getSelectedItem().getText();
			Torre = Torre.replace(/ /g, "");
			if (etapa) {
				etapa = "0".concat(etapa.substr(etapa.length - 1, 1));
				sProjectId = sProjectId.concat(etapa);
			}
			var that = this;
			this.showBusyDialog(function () {
				var sFilter = "$filter= Proye eq '" + sProjectId +
					"' and (Matkl eq 'Z00800001' or Matkl eq 'Z00800002' or Matkl eq 'Z00800005' or Matkl eq 'Z00800006' or Matkl eq 'Z00800007')";
				that.erp.read("DisponibilidadSet?" + sFilter + "&sap-language=ES", null, null, false,
					function (oData) {
						$.each(oData.results, function (index, oUnity) {
							oUnity.Netwr = Number(oUnity.Netwr);
							oUnity.Matnr = '00000000' + oUnity.Matnr;
							oUnity.Area = oUnity.Area.split(",").join(".");
							aUnities[index] = {
								selected: false,
								adicional: oUnity
							};
						});
					}
				);
				console.log(aUnities);
				oModel.setProperty("/unidades", aUnities);
				var oDatos = [];
				var pisos = [];
				var available = 0;
				var optional = 0;

				var TotalSold = 0;
				that.getView().byId("Pisos").setVisible(true);
				that.getView().byId("resumenInv").setVisible(true);
				that.getView().byId("distribucionMt").setVisible(true);
				for (var i = 0; i < aUnities.length; i++) {
					var sold = 0;
					var numeroR = that.consultarFechaReserva(aUnities[i].adicional.Matnr);
					sold = that.validateSoldUnities(aUnities[i].adicional.Matnr);
					TotalSold = TotalSold + sold;
					var dato = pisos.filter(function (Txt) {
						return Txt.Pisot === aUnities[i].adicional.Pisot;
					});
					if (dato.length === 0) {
						pisos.push(aUnities[i].adicional);
					}
					if (sold === 0) {
						available = available + 1;
					}
					if (numeroR.length === 1 || numeroR.length === 2 || numeroR.length === 3) {
						optional = optional + 1;
					}
				}
				console.log(pisos);
				//Setear las propiedades a la grafica de unidades.
				that.getView().byId("available").setValue(available);
				that.getView().byId("optionals").setValue(optional);
				that.getView().byId("sold").setValue(TotalSold);
				oModel.setProperty("/pisos", pisos);
			});
		},
		//Función para validar las unidades reservadas vendidas.
		validateSoldUnities: function (Matnr) {
			var that = this;
			var contador = 0;
			var sSelectFields = "$select= ID_HOJA_NEGOCIO";
			var sSelectField = "$select= ESTADO";
			var unidades = [];
			var sFilter = "$filter= MARA_MATNR eq '" + Matnr + "'";
			that.hana.read("T_HOJA_NEGOCIOS_ADICIONES?" + sSelectFields + "&" + sFilter, null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						unidades = oData.results;
					}
				});
			for (var j = 0; j < unidades.length; j++) {
				var sFilter1 = "$filter= ID_HOJA_NEGOCIO eq " + unidades[j].ID_HOJA_NEGOCIO + "";
				that.hana.read("T_HOJA_NEGOCIO?" + sSelectField + "&" + sFilter1, null, null, false,
					function fnSuccess(oData) {
						if (oData.results.length > 0) {
							for (var i = 0; i < oData.results.length; i++) {
								if (oData.results[i].ESTADO === 0 || oData.results[i].ESTADO === 1 || oData.results[i].ESTADO === 2 || oData.results[i].ESTADO ===
									3 || oData.results[i].ESTADO === 4 || oData.results[i].ESTADO === 5 || oData.results[i].ESTADO === 6 || oData.results[i]
									.ESTADO === 7 || oData.results[i].ESTADO === 11) {
									contador = contador + 1;
								}
							}
						}
					});
			}
			return contador;
		},
		//Función para actualizar las unidades en las tablas de reservas.
		ActualizarUnidades: function () {
			var oModel = this.getView().getModel();
			var oUnidades = oModel.getProperty("/unidades");
			var oUnidadesPiso = oModel.getProperty("/filtroUnidades");
			var that = this;
			this.showBusyDialog(function () {
				for (var i = 0; i < oUnidades.length; i++) {
					for (var j = 0; j < oUnidadesPiso.length; j++) {
						if (oUnidadesPiso[j].material === oUnidades[i].adicional.Matnr) {
							var tipo;
							var numeroR = that.consultarFechaReserva(oUnidades[i].adicional.Matnr);
							if (numeroR.length === 2 || numeroR.length === 1 || numeroR.length === 3) {
								tipo = "Emphasized";
							}
							var unidadesVendidas = that.validateSoldUnities(oUnidades[i].adicional.Matnr);
							if (unidadesVendidas > 0) {
								tipo = "Reject";
							} else if (unidadesVendidas === 0 && numeroR.length === 0) {
								tipo = "Accept";
							}
							oUnidadesPiso[j].type = tipo;
						}
					}
				}
				oModel.setProperty("/filtroUnidades", oUnidadesPiso);
			});
		},
		//Función para traer los clientes de C4C.
		LeerClientes: function (oEvent) {
			//1015804
			var oModel = this.getView().getModel();
			var tipoId = oModel.getProperty("/tipoIdentificacion");
			var proyecto = this.getView().byId("cmbProyecto").getSelectedKey();
			var documento = this.getView().byId("idLead").getValue();
			if (!tipoId) {
				sap.m.MessageBox.warning("Por favor diligencie el campo Tipo de Identificación.");
				return;
			} else if (!proyecto) {
				sap.m.MessageBox.warning("Por favor diligencie el campo Proyecto.");
				return;
			} else if (!documento) {
				sap.m.MessageBox.warning("Por favor diligencie el documento del Lead.");
				return;
			}
			//var documento = oEvent.getSource().getValue();
			var sFilter = "$filter= CustomerID eq '" + documento + "'";
			var aAsesor = [];
			var that = this;
			this.showBusyDialog(function () {
				that.c4c.read("IndividualCustomerCollection?" + sFilter + "&sap-language=ES", null, null, false,
					function fnSuccess(oData) {
						sap.ui.core.BusyIndicator.show(0);
						if (oData.results.length > 0) {
							aAsesor = oData.results;
							var nombre = aAsesor[0].FirstName + " " + aAsesor[0].MiddleName + " " + aAsesor[0].LastName + " " + aAsesor[0].AdditionalLastName;
							that.getView().byId("nombreLead").setText(nombre);
						} else {
							sap.m.MessageBox.warning("El Lead no se encuentra creado por favor revisar.");
							that.getView().byId("nombreLead").setText("");
							oEvent.getSource().setValue();
						}
						sap.ui.core.BusyIndicator.hide();
					});
				console.log(aAsesor);
			});

		},
		//Función para consultar las oportunidades.
		consultarOportunidad: function () {
			var tipoIdentificacion = this.getView().byId("tipoId").getSelectedKey();
			var identificacion = this.getView().byId("idLead").getValue();
			var proyecto = this.getView().byId("cmbProyecto").getSelectedKey();
			var proyecto_c4c;
			var sFilter = "$filter= PROYECTO_T179 eq '" + proyecto + "'";
			this.hana.read("T_PROYECTOS_C4C?" + sFilter, null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						proyecto_c4c = oData.results[0].PROYECTO_C4C;
					}
				});
			var sFilter1 = "$filter= TipodeIdentificacinOportunidad eq '" + tipoIdentificacion + "' and NdeIdentificacinOportunidad eq " +
				identificacion + " and PROYECTO eq '" + proyecto_c4c + "'";
			this.c4c.read("OpportunityCollection? " + sFilter1 + "&sap-language=ES", null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						console.log(oData.results);
					}
				});
		},
		//Función para traer los Tipos de Identificación de C4C.
		tipoIdentificacion: function () {
			var tipoIdentificacion = [];
			this.c4c.read("OpportunityTipodeIdentificacinOportunidadCollection?sap-language=ES", null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						tipoIdentificacion = oData.results;
					}
				});
			console.log(tipoIdentificacion);
			this.getView().getModel().setProperty("/tipoIdentificacion", tipoIdentificacion);
		},
		//Función para tomar intervalo de fechas.
		handleCalendarSelect: function (oEvent) {
			var oModel = this.getView().getModel();
			var oCalendar = oEvent.getSource();
			var fechas = oCalendar.getSelectedDates()[0];
			var fechaInicio = fechas.getStartDate();
			var fechaFin = fechas.getEndDate();
			var bandera;
			if (fechaFin) {
				bandera = this.determinarDias(oEvent);
			}
			if (!bandera) {
				oModel.setProperty("/reserva/FECHA_INICIAL", fechaInicio);
				oModel.setProperty("/reserva/FECHA_FINAL", fechaFin);
			} else {
				oModel.setProperty("/reserva/FECHA_INICIAL", null);
				oModel.setProperty("/reserva/FECHA_FINAL", null);
			}
		},
		//Evaluar el número de dias seleccionado para reservar la unidad.
		determinarDias: function (oEvent) {
			var oCalendar = oEvent.getSource();
			var oModel = this.getView().getModel();
			var fechas = oCalendar.getSelectedDates()[0];
			var fechaInicio = fechas.getStartDate();
			var fechaFin = fechas.getEndDate();
			var flag = false;
			var dias = 3;
			var fecha1 = this.formateDate(fechaInicio);
			var fecha2 = this.formateDate(fechaFin);
			var resta = this.restaFechas(fecha1, fecha2);
			if (resta > dias) {
				sap.m.MessageBox.error("La reserva no puede superar los " + dias + " días.");
				fechas.setStartDate(null);
				fechas.setEndDate(null);
				flag = true;
			}
			return flag;
		},
		//Función para formatear fecha "dd/mm/yyy".
		formateDate: function (date) {
			var fechaCesionFormat;
			var year;
			var month;
			var day;
			var dateformat;
			if (date !== null) {
				fechaCesionFormat = new Date(date);
				year = fechaCesionFormat.getFullYear();
				month = fechaCesionFormat.getMonth() + 1;
				day = fechaCesionFormat.getDate();
				if ((day + "").length === 1) {
					day = "0" + day;
				}
				if ((month + "").length === 1) {
					month = "0" + month;
				}
				dateformat = day + "/" + (month) + "/" + year;
				if (year === 0) {
					dateformat = null;
				}
				return dateformat;
			} else {
				dateformat = '';
				return dateformat;
			}
		},
		//Función para formatear fecha "yyyy/mm/dd".
		formateDate1: function (date) {
			var fechaCesionFormat;
			var year;
			var month;
			var day;
			var dateformat;
			if (date !== null) {
				fechaCesionFormat = new Date(date);
				year = fechaCesionFormat.getFullYear();
				month = fechaCesionFormat.getMonth() + 1;
				day = fechaCesionFormat.getDate();
				if ((day + "").length === 1) {
					day = "0" + day;
				}
				if ((month + "").length === 1) {
					month = "0" + month;
				}
				dateformat = year + "/" + (month) + "/" + day;
				if (year === 0) {
					dateformat = null;
				}
				return dateformat;
			} else {
				dateformat = '';
				return dateformat;
			}
		},
		//Función para hallar diferencia de dás en dos fechas.
		restaFechas: function (f1, f2) {
			var aFecha1 = f1.split('/');
			var aFecha2 = f2.split('/');
			var fFecha1 = Date.UTC(aFecha1[2], aFecha1[1] - 1, aFecha1[0]);
			var fFecha2 = Date.UTC(aFecha2[2], aFecha2[1] - 1, aFecha2[0]);
			var dif = fFecha2 - fFecha1;
			var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
			return dias;
		},
		//Función para abrir el modal de Asignación de Unidades.
		Asignacion: function () {
			//this.LeerClientes();
			var that = this;
			this.showBusyDialog(function () {
				var sProjectId = that.getView().byId("cmbProyecto").getSelectedKey();
				if (!sProjectId) {
					sap.m.MessageBox.warning("El campo de Proyecto es obligatorio.");
					return;
				}
				if (!that._oOrden) {
					that._oOrden = sap.ui.xmlfragment("ui5.jm.ReservaInmueble.view.fragment.AvailableProducts", that);
					that.getView().addDependent(that._oOrden);
				}
				that._oOrden.open();
				var dato = that.getView().byId("cmbProyecto").getSelectedItem().getText();
				var nombre_lead = that.getView().byId("nombreLead").getText();
				sap.ui.getCore().byId("proyecto").setText(dato);
				sap.ui.getCore().byId("Nombre_lead").setText(nombre_lead);
				that.leerAsesores();
				that.leerEstados();
				that.unityType();
			});
		},
		//Función para cerrar el modal de Asignación de Unidades.
		closeAsignacion: function () {
			var oModel = this.getView().getModel();
			this._oOrden.close();
			oModel.setProperty("/unidadesSel", []);
			oModel.setProperty("/unidadesMod", []);
			var reserva = {
				ID_RESERVA: 1,
				PROYECTO: "",
				ETAPA: "",
				TORRE: "",
				LEAD_IDENTIFICACION: null,
				LEAD_NOMBRE: "",
				ASESOR: null,
				ESTADO_RESERVA: null,
				FECHA_INICIAL: null,
				FECHA_FINAL: null,
				FECHA_CREACION: new Date(),
				MEDIO: "",
				ID_OPORTUNIDAD: 0
			};
			oModel.setProperty("/reserva", reserva);
			sap.ui.getCore().byId("tipoUnidad").setSelectedKey(null);
			sap.ui.getCore().byId("reservedDateFrom").setDateValue(null);
			sap.ui.getCore().byId("reservedDateTo").setDateValue(null);
			sap.ui.getCore().byId("numero_reservas").setText("");
		},
		//Función para la asignación de las tablas de unidades disponibles y seleccionadas en el modal.
		AsignarUnidad: function (oEvent) {
			var oModel = this.getView().getModel();
			var path = oEvent.getSource()._aSelectedPaths[0];
			var oFechas = this.consultarFechaReserva(oModel.getProperty(path).adicional.Matnr);
			if (oFechas.length > 0) {
				oFechas[0].FECHA_INICIAL = new Date(oFechas[0].FECHA_INICIAL.toUTCString().slice(0, -4));
				oFechas[oFechas.length - 1].FECHA_FINAL = new Date(oFechas[oFechas.length - 1].FECHA_FINAL.toUTCString().slice(0, -4));
				sap.ui.getCore().byId("numero_reservas").setText(oFechas.length);
				sap.ui.getCore().byId("reservedDateFrom").setDateValue(oFechas[0].FECHA_INICIAL);
				sap.ui.getCore().byId("reservedDateTo").setDateValue(oFechas[oFechas.length - 1].FECHA_FINAL);
				/*var calendario = sap.ui.getCore().byId("calendar").getSelectedDates()[0];
				calendario.setStartDate(oFechas[0].FECHA_INICIAL);
				calendario.setEndDate(oFechas[oFechas.length - 1].FECHA_FINAL);*/
			} else {
				sap.ui.getCore().byId("numero_reservas").setText(0);
				sap.ui.getCore().byId("reservedDateFrom").setDateValue(null);
				sap.ui.getCore().byId("reservedDateTo").setDateValue(null);
			}
			var nuevasUnidades = oModel.getProperty("/unidadesSel");
			var Unidades_disponibles = oModel.getProperty("/unidadesMod");
			if (!nuevasUnidades[0]) {
				nuevasUnidades = [];
			}
			if (nuevasUnidades.length === 0) {
				nuevasUnidades.push(oModel.getProperty(path));
				oModel.setProperty("/unidadesSel", nuevasUnidades);
				var posicion = parseFloat(path.substr(path.length - 1));
				delete Unidades_disponibles[posicion];
				Unidades_disponibles.sort();
				nuevasUnidades.sort();
				oModel.setProperty("/unidadesMod", Unidades_disponibles);
			} else {
				for (var i = 0; i < nuevasUnidades.length; i++) {
					if (nuevasUnidades.length > 0) {
						if (nuevasUnidades[i]) {
							if (oModel.getProperty(path).adicional.Tunidadt === "Apartamento" && nuevasUnidades[i].adicional.Tunidadt === "Apartamento") {
								sap.m.MessageBox.warning("Solo se puede seleccionar una unidad Principal");
							} else {
								nuevasUnidades.push(oModel.getProperty(path));
								oModel.setProperty("/unidadesSel", nuevasUnidades);
								var posicion = parseFloat(path.substr(path.length - 1));
								delete Unidades_disponibles[posicion];
								Unidades_disponibles.sort();
								nuevasUnidades.sort();
								oModel.setProperty("/unidadesMod", Unidades_disponibles);
							}
						}
					}
				}
			}

		},
		//Función para la asignación de las tablas de unidades disponibles y seleccionadas en el modal.
		AsignarUnidad1: function (oEvent) {
			var oModel = this.getView().getModel();
			var path = oEvent.getSource()._aSelectedPaths[0];
			var unidades_disponibles = oModel.getProperty("/unidadesMod");
			unidades_disponibles.push(oModel.getProperty(path));
			oModel.setProperty("/unidadesMod", unidades_disponibles);
			var nuevasUnidades = oModel.getProperty("/unidadesSel");
			var posicion = parseFloat(path.substr(path.length - 1));
			delete nuevasUnidades[posicion];
			nuevasUnidades.sort();
			unidades_disponibles.sort();
			oModel.setProperty("/unidadesSel", nuevasUnidades);
		},
		//Función para consultar información de reserva.
		historialData: function (oEvent) {
			var oModel = this.getView().getModel();
			oModel.setProperty("/historial", []);
			var Matnr = oEvent.getSource().getParent().getCells()[1].getText();
			var unidades = [];
			var that = this;
			this.showBusyDialog(function () {
				var sFilter = "$filter= MATERIAL_UNIDAD eq '" + Matnr + "'";
				that.hana.read("T_RESERVA_UNIDADES?" + sFilter, null, null, false,
					function fnSuccess(oData) {
						if (oData.results.length > 0) {
							for (var j = 0; j < oData.results.length; j++) {
								unidades.push(oData.results[j]);
							}
						}
					});
				var Datos = [];
				for (var i = 0; i < unidades.length; i++) {
					var Filter = "$filter= ID_RESERVA eq " + unidades[i].ID_RESERVA + "";
					that.hana.read("T_RESERVA_INMUEBLE?" + Filter, null, null, false,
						function fnSuccess(oData) {
							if (oData.results.length > 0) {
								for (var j = 0; j < oData.results.length; j++) {
									if (oData.results[j].ESTADO_RESERVA !== 4) {
										oData.results[j].FECHA_INICIAL = new Date(oData.results[j].FECHA_INICIAL.toUTCString().slice(0, -4));
										oData.results[j].FECHA_FINAL = new Date(oData.results[j].FECHA_FINAL.toUTCString().slice(0, -4));
										Datos.push(oData.results[j]);
									}
								}
							}
						});
				}
				oModel.setProperty("/historial", Datos);
				that.historialModal();
			});
		},
		//Función para abrir el modal de Asignación de Unidades.
		historialModal: function () {
			if (!this._oHistorial) {
				this._oHistorial = sap.ui.xmlfragment("ui5.jm.ReservaInmueble.view.fragment.historial", this);
				this.getView().addDependent(this._oHistorial);
			}
			this._oHistorial.open();
		},
		//Función para cerrar el modal de Asignación de Unidades.
		cerrarhistorialModal: function () {
			this.ActualizarUnidades();
			this._oHistorial.close();
		},
		//Función para cancelar la Reserva y recalcular las fechas de la reserva.
		cancelarReserva: function (oEvent) {
			var oModel = this.getView().getModel();
			var that = this;
			var datos = oModel.getProperty("/historial");
			var fecha_Icancelada;
			var fecha_Fcancelada;
			var fechaI_anterior;
			var fechaF_anterior;
			var posicion;
			var deleteIndex;
			var reserva = parseFloat(oEvent.getSource().getParent().getCells()[6].getText());
			for (var i = 0; i < datos.length; i++) {
				if (datos[i].ID_RESERVA === reserva) {
					if (i === datos.length - 1) {
						deleteIndex = i;
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "ESTADO_RESERVA",
							valueToUpdate = 4,
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = reserva;
						that.funcionActualizar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);

					} else {
						var index = i + 1;
						fecha_Icancelada = datos[i].FECHA_INICIAL;
						fecha_Fcancelada = datos[i].FECHA_FINAL;
						fechaI_anterior = datos[index].FECHA_INICIAL;
						fechaF_anterior = datos[index].FECHA_FINAL;
						posicion = i;
						deleteIndex = i;
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "ESTADO_RESERVA",
							valueToUpdate = 4,
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = reserva;
						that.funcionActualizar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
					}
				}
				if (posicion === 0) {
					var index = posicion + 1;
					if (datos[index]) {
						datos[index].FECHA_INICIAL = fecha_Icancelada;
						datos[index].FECHA_FINAL = fecha_Fcancelada;
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_INICIAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_INICIAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_FINAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_FINAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
					}
					var index1 = posicion + 2;
					if (datos[index1]) {
						datos[index1].FECHA_INICIAL = fechaI_anterior;
						datos[index1].FECHA_FINAL = fechaF_anterior;
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_INICIAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_INICIAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_FINAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_FINAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
					}
				} else if (posicion === 1) {
					var index = posicion + 1;
					if (datos[index]) {
						datos[index].FECHA_INICIAL = fecha_Icancelada;
						datos[index].FECHA_FINAL = fecha_Fcancelada;
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_INICIAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_INICIAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_FINAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_FINAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
					}
				} else if (posicion === 2) {
					var index = posicion + 1;
					if (datos[index]) {
						datos[index].FECHA_INICIAL = fecha_Icancelada;
						datos[index].FECHA_FINAL = fecha_Fcancelada;
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_INICIAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_INICIAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
						var sTableName = "T_RESERVA_INMUEBLE",
							sFieldName = "FECHA_FINAL",
							valueToUpdate = that.formateDate1(datos[index].FECHA_FINAL),
							sFieldKey = "ID_RESERVA",
							sFieldKeyValue = datos[i].ID_RESERVA;
						that.updateDate(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
					}
				}
			}
			datos.splice("" + deleteIndex, 1);
			oModel.setProperty("/historial", datos);
			sap.m.MessageBox.warning("Las fechas de las reservas se han recalculado.");
			sap.m.MessageBox.success("La reserva de la unidad se ha cancelado correctamente");
		},
		//Función que permite actualizar un campo específico en una tabla de SCP.
		funcionActualizar: function (tabla, campo, valor, llave, valor_llave) {
			var oInfo = {
				sTableName: tabla,
				sFieldName: campo,
				valueToUpdate: valor,
				sFieldKey: llave,
				sFieldKeyValue: valor_llave
			};

			jQuery.ajax({
				url: this.xsjs + "xsjs/impressions_center/upd_anyone_field.xsjs?P_TABLA=" + oInfo.sTableName + "&P_CAMPO=" + oInfo
					.sFieldName +
					"&P_VALOR=" + oInfo.valueToUpdate + "&P_CAMPOPK=" + oInfo.sFieldKey + "&P_VALORPK=" + oInfo.sFieldKeyValue,
				method: 'GET',
				dataType: 'json',
				async: false,
				success: function (result) {
					console.log(["Proceso ok en actualizacíon de motivo: ", result]);
				},
				error: function (err) {
					console.log(["Error actualizando el motivo: ", err]);
				}
			});
		},
		//Función que permite actualizar una fecha específica en una tabla de SCP.
		updateDate: function (tabla, campo, valor, llave, valor_llave) {
			var oInfo = {
				sTableName: tabla,
				sFieldName: campo,
				valueToUpdate: valor,
				sFieldKey: llave,
				sFieldKeyValue: valor_llave
			};

			jQuery.ajax({
				url: this.xsjs + "xsjs/impressions_center/upd_date_field.xsjs?P_TABLA=" + oInfo.sTableName + "&P_CAMPO=" + oInfo
					.sFieldName +
					"&P_VALOR=" + oInfo.valueToUpdate + "&P_CAMPOPK=" + oInfo.sFieldKey + "&P_VALORPK=" + oInfo.sFieldKeyValue,
				method: 'GET',
				dataType: 'json',
				async: false,
				success: function (result) {
					console.log(["Proceso ok en actualizacíon de motivo: ", result]);
				},
				error: function (err) {
					console.log(["Error actualizando el motivo: ", err]);
				}
			});
		},
		//Función para validar el número de Reservas.
		validarReservas: function () {
			var oModel = this.getView().getModel();
			var data = [];
			var flag = false;
			var unidad = oModel.getProperty("/unidadesSel");
			for (var i = 0; i < unidad.length; i++) {
				var sFilter = "$filter= MATERIAL_UNIDAD eq '" + unidad[i].adicional.Matnr + "'";
				this.hana.read("T_RESERVA_UNIDADES?" + sFilter, null, null, false,
					function fnSuccess(oData) {
						if (oData.results.length >= 3) {
							sap.m.MessageBox.warning("La unidad a reservar ya tiene las 3 reservaciones permitidas. Por favor revise");
							flag = true;
						}
					});
			}
			return flag;
		},
		//Función para consultar fechas de reserva de Unidad.
		consultarFechaReserva: function (Matnr) {
			var unidades = [];
			var sFilter = "$filter= MATERIAL_UNIDAD eq '" + Matnr + "'";
			this.hana.read("T_RESERVA_UNIDADES?" + sFilter, null, null, false,
				function fnSuccess(oData) {
					if (oData.results.length > 0) {
						for (var j = 0; j < oData.results.length; j++) {
							unidades.push(oData.results[j]);
						}
					}
				});
			var fechas = [];
			for (var i = 0; i < unidades.length; i++) {
				var sSelectFields = "$select= FECHA_INICIAL,FECHA_FINAL,ESTADO_RESERVA";
				var Filter = "$filter= ID_RESERVA eq " + unidades[i].ID_RESERVA + "";
				this.hana.read("T_RESERVA_INMUEBLE?" + sSelectFields + "&" + Filter, null, null, false,
					function fnSuccess(oData) {
						if (oData.results.length > 0) {
							for (var j = 0; j < oData.results.length; j++) {
								if (oData.results[j].ESTADO_RESERVA !== 4) {
									fechas.push(oData.results[j]);
								}
							}
						}
					});
			}
			console.log(fechas);
			return fechas;
		},
		//Función para guardar la reserva.
		guardarReserva: function () {
			var oModel = this.getView().getModel();
			var proyecto = this.getView().byId("cmbProyecto").getSelectedKey();
			var etapa = this.getView().byId("cmbProyecto").getSelectedItem().getText();
			var identificacion_lead = this.getView().byId("idLead").getValue();
			var nombre_lead = this.getView().byId("nombreLead").getText();
			var that = this;
			etapa = "0".concat(etapa.substr(etapa.length - 1, 1));
			var oData = oModel.getProperty("/reserva");
			if (!oData.ASESOR) {
				sap.m.MessageBox.warning("El campo de Asesor es obligatorio.");
				return;
			}
			if (!oData.FECHA_INICIAL) {
				sap.m.MessageBox.warning("Las fechas de Reserva son obligatorias.");
				return;
			}
			if (!oData.FECHA_FINAL) {
				sap.m.MessageBox.warning("Las fechas de Reserva son obligatorias.");
				return;
			}
			oData.PROYECTO = proyecto;
			oData.ETAPA = etapa;
			var fechaInicialGuardada = sap.ui.getCore().byId("reservedDateFrom").getDateValue();
			var fechaFinalGuardada = sap.ui.getCore().byId("reservedDateTo").getDateValue();
			if (this.formateDate(oData.FECHA_INICIAL) >= this.formateDate(fechaInicialGuardada) && this.formateDate(oData.FECHA_INICIAL) <=
				this.formateDate(fechaFinalGuardada)) {
				sap.m.MessageBox.warning("El intervalo de fechas a reservar no se encuentra disponible.");
				oModel.setProperty("/reserva/FECHA_INICIAL", null);
				oModel.setProperty("/reserva/FECHA_FINAL", null);
				return;
			} else if (this.formateDate(oData.FECHA_FINAL) >= this.formateDate(fechaInicialGuardada) && this.formateDate(oData.FECHA_FINAL) <=
				this.formateDate(fechaFinalGuardada)) {
				sap.m.MessageBox.warning("El intervalo de fechas a reservar no se encuentra disponible.");
				oModel.setProperty("/reserva/FECHA_INICIAL", null);
				oModel.setProperty("/reserva/FECHA_FINAL", null);
				return;
			}
			if (identificacion_lead) {
				oData.LEAD_IDENTIFICACION = identificacion_lead;
			}
			if (nombre_lead) {
				oData.LEAD_NOMBRE = nombre_lead;
			}
			oData.FECHA_INICIAL = new Date(oData.FECHA_INICIAL);
			oData.FECHA_FINAL = new Date(oData.FECHA_FINAL);
			var oUnidades = oModel.getProperty("/unidadesSel");
			oData.ESTADO_RESERVA = 2;
			oData.TORRE = oUnidades[0].adicional.Torret;
			console.log(oData);
			var id_reserva;
			this.hana.create("/T_RESERVA_INMUEBLE", oData, {
				success: function (oResponse) {
					id_reserva = oResponse.ID_RESERVA;
				},
				error: function (err) {
					console.log(err);
				}
			});
			for (var i = 0; i < oUnidades.length; i++) {
				var flag = this.consultarFechaReserva(oUnidades[i].adicional.Matnr);
				if (flag.length === 3) {
					sap.m.MessageBox.warning("La unidad a reservar ya tiene las 3 reservaciones permitidas. Por favor revise");
					oModel.setProperty("/reserva/FECHA_INICIAL", null);
					oModel.setProperty("/reserva/FECHA_FINAL", null);
					return;
				}
				var arreglo = {
					ID: 1,
					ID_RESERVA: id_reserva,
					MATERIAL_UNIDAD: oUnidades[i].adicional.Matnr
				};
				this.hana.create("/T_RESERVA_UNIDADES", arreglo, {
					success: function (oResponse) {
						sap.m.MessageBox.success("Se ha(n) reservado la(s) unidad(es) correctamente.");
						that.ActualizarUnidades();
					},
					error: function (err) {
						console.log(err);
					}
				});
			}
			var reserva = {
				ID_RESERVA: 1,
				PROYECTO: "",
				ETAPA: "",
				TORRE: "",
				LEAD_IDENTIFICACION: null,
				LEAD_NOMBRE: "",
				ASESOR: null,
				ESTADO_RESERVA: null,
				FECHA_INICIAL: null,
				FECHA_FINAL: null,
				FECHA_CREACION: new Date(),
				MEDIO: "",
				ID_OPORTUNIDAD: 0
			};
			oModel.setProperty("/reserva", reserva);
			this.closeAsignacion();

		}
	});
});